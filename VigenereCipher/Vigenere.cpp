#include<iostream>
#include<string>
#include<vector>
using namespace std;
string arr = "abcdefghijklmnopqrstuvwxyz ";
int main()
{
	string plainText = "", key="";   //initial key and plain text as strings
	vector<char> keyCode, plain,cipher;     //transfer key and text to character
	vector<int> ArrCode,ArrPlain,ArrCipher;                 //array to store indexes of cipher text elememts
	int keyLength = 0,cipherIndex=0,plainIndex=0;               
	cout << "Plain text: ";
	getline(cin, plainText);         //enter the plain text
	cout << endl;
	cout << "Key: ";                 
	getline(cin, key);                //enter the key
	cout << endl;
	for (int i = 0; i < key.length(); i++)  //Populate the key vector
	{
		keyCode.push_back(key[i]);
	}
	for (int i = 0; i < key.length(); i++)  //populate
	{
		plain.push_back(plainText[i]);
	}
	int newLength = key.size();
	int oldlength = key.size();
	while (newLength != plainText.length())           //loop to duplicate the key to be equal the length of the string
	{
		keyCode.push_back(key[keyLength]);            //access the elements of the key to the key vector of equal length to the plain text
		keyLength++;                                  
		if (keyLength >= oldlength)                   //test if the size of the vector are equal to the length of the initial key
		{
			keyLength = 0;                            //reset the populating index to zero, to start populating the key from the first element
		}
		newLength++;                                  //increment to check if the lenght of the key is equal to that of the plain text
	}
	while (cipherIndex != plainText.length())
	{
		for (int k = 0; k < arr.size(); k++)
		{
			if (keyCode[cipherIndex] == arr[k])      //compare the each character in alphabet and key code
			{
				ArrCode.push_back(k % 26);            //add the sum of the mod of the indexes to integer vector
			}
		}
		cipherIndex++;                                  //increment to reach the end of the key/ plain text
	}
	while (plainIndex != plainText.length())
	{
		for (int k = 0; k < arr.size(); k++)
		{
			if (plainText[plainIndex] == arr[k])             //compare the each character in plain text and alphabet
			{
				ArrPlain.push_back(k % 26);            //add the sum of the mod of the indexes to integer vector
			}
		}
		plainIndex++;                                  //increment to reach the end of the key/ plain text
	}
	for (int k = 0; k < ArrPlain.size(); k++)
	{
		ArrCipher.push_back((ArrPlain[k] + ArrCode[k]) % 26);
	}
	for (int k = 0; k < ArrPlain.size(); k++)
	{
		cipher.push_back(arr[ArrCipher[k] % 26]);
	}
	for (int i = 0; i < cipher.size(); i++)
	{
		cout << cipher[i];
	}
	cout <<endl;

	system("pause");
	return 0;
}